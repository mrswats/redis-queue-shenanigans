import time

import task

result = task.long_task.delay()

is_ready = result.ready()

while not is_ready:
    print("Not yet ready")
    time.sleep(1)

    is_ready = result.ready()

print(result.result)
