import time

import redis

QUEUE_NAME = "main-task-queue"


def main() -> int:
    r = redis.Redis(decode_responses=True)

    while True:
        resp = r.lrange(QUEUE_NAME, 0, -1)
        print(resp)
        time.sleep(1)

    print(resp)
    return 0


if __name__ == "__main__":
    raise SystemExit(main())
