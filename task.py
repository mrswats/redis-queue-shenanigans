import time

from celery import Celery


app = Celery(broker="redis:///0", backend="redis:///0")


@app.task
def long_task(*args, **kwargs) -> float:
    print("Starting task with args: %s, %s", args, kwargs)
    for _ in range(30):
        time.sleep(1)
        print(30 - _)

    return 3.14


@app.task
def long_task_2(*args, **kwargs) -> float:
    print("Starting task2 with args: %s, %s", args, kwargs)
    for _ in range(10):
        time.sleep(1)
        print(10 - _)

    return 3.14
