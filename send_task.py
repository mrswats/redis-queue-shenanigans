from celery import Celery

app = Celery(broker="redis:///0")

app.send_task("task.long_task", args=[1, 2, 3], kwargs={"foo": "bar"})
