import argparse
import json

import redis

QUEUE_NAME = "main-task-queue"


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument("message", type=str)
    args = parser.parse_args()

    r = redis.Redis()
    response = r.lpush(QUEUE_NAME, json.dumps({"message": args.message}))

    print(response)

    return 0


if __name__ == "__main__":
    raise SystemExit(main())
