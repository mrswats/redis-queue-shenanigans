import logging
import random
import time
from datetime import datetime

import redis

from task import long_task

random.seed()


class MessageProcessingError(Exception):
    pass


class Consumer:
    def __init__(
        self,
        redis_client: redis.Redis,
        queue_name: str,
        temp_queue_name: str,
    ):
        self.client = redis_client
        self.queue_name = queue_name
        self.temp_queue_name = temp_queue_name

    def consume(self) -> None:
        while True:
            message = self.client.rpoplpush(self.queue_name, self.temp_queue_name)
            if message is None:
                logging.info("no message in queue")
                self.wait_for_next_message()
                continue

            self._consume_queue(message)

    def _put_message_back_in_queue(self, message) -> None:
        try:
            self.client.lrem(self.temp_queue_name, 1, message)
        finally:
            self.client.lpush(self.queue_name, message)

        logging.error("Message back into main queue: %s", message)

    def _consume_queue(self, message) -> None:
        try:
            logging.info("Processing message: %s", message)
            if not self.process_message(message):
                raise MessageProcessingError("error in message")
        except MessageProcessingError:
            self._put_message_back_in_queue(message)
        except KeyboardInterrupt:
            self._put_message_back_in_queue(message)
            raise SystemExit(1)
        else:
            self.client.lrem(self.temp_queue_name, 1, message)
            logging.info("Message processed successfully: %s", message)

    @staticmethod
    def wait_for_next_message() -> None:
        time.sleep(1)

    @staticmethod
    def process_message(message: str) -> bool:
        raise NotImplementedError("This method is to be implemented in child classes")


class DummyConsumer(Consumer):
    @staticmethod
    def process_message(message: str) -> bool:
        long_task.delay(message, time=datetime.now())
        return True


def main() -> int:
    logging.basicConfig(level=logging.DEBUG)
    client = redis.Redis(decode_responses=True)
    consumer: Consumer = DummyConsumer(client, "main-task-queue", "temp-task-queue")
    consumer.consume()
    return 0


if __name__ == "__main__":
    raise SystemExit(main())
